#!/bin/bash
#=========================================================================
#
# Script to run CTRIP assimilation with forcings at a monthly time step
#
# USAGE
#   ./script_assim.sh EXP OBS OPENLOOP
#
# EXP      : experience name (by default: EXP=ASSIM)
# OBS      : observation directory (by default: OBS=OBS)
# OPENLOOP : 1 if open-loop (empty observations), 0 else
#
#=========================================================================
#set -x

REGION=Spain
EXP=${1:-TRIP_OL}
OBS=${2:-OBS_empty}
OPENLOOP=${3:-1}

TRIPNAML=TRIP_OPTIONS.nam
FORCINGTARNAME=ISBA_ENS
FORCINGNAME=ISBA_DIAG_CUMUL
NENS=25

#============================================
# simulation time span
#============================================
YYYYMM_beg=199901
YYYYMM_end=200012
DDHH=0100

#============================================
# directories and links
#============================================
VER=LXifort-SFX-V9-0-MPIAUTO-OMP-O2-X0-MYTRIP
TRUNKDIR=$HOME/src/SURFEX_V9_DEV/Surfex_Git2
RIVEXE=$TRUNKDIR/exe/TRIP_ASSIM-$VER

SCRATCHDIR=$WORKDIR/SWOT/$REGION
OBSDIR=$SCRATCHDIR/OBS/$OBS
FORCINGDIR=/home/muniers/SWOT/$REGION/RUN_ISBA # on hendrix
RESULTSDIR=/home/muniers/SWOT/$REGION/$EXP     # on hendrix

#============================================
# create directories and copy files
#============================================
RUNDIR=$(pwd)
mkdir -p $SCRATCHDIR/$EXP/ENS
mkdir -p $SCRATCHDIR/$EXP/TRIP_PREP
rm -rf $SCRATCHDIR/RUN/$EXP
mkdir -p $SCRATCHDIR/RUN/$EXP
cd $SCRATCHDIR/RUN/$EXP

# get params
ln -s $RUNDIR/PGD_PREP/TRIP_PARAM.nc TRIP_PARAM.nc
# get restart
IENS=1
while [ $IENS -le $NENS ] ; do
  IENS0=`printf %03d $IENS`
  ln -s $RUNDIR/PGD_PREP/TRIP_PREP_${YYYYMM_beg}${DDHH}.nc TRIP_PREP_${IENS0}.nc
  #ln -s $RUNDIR/PGD_PREP/TRIP_PREP_${YYYYMM_beg}${DDHH}_${IENS0}.nc TRIP_PREP_${IENS0}.nc
  IENS=$((IENS+1))
done
# get option & parameters files
cp $RUNDIR/OPTIONS/$TRIPNAML TRIP_OPTIONS.nam
# get executables
ln -s $RUNDIR/SCRIPTS/GET_FORCING.sh .
ln -s $RUNDIR/SCRIPTS/PUT_RESULT.sh .
ln -s $RUNDIR/SCRIPTS/PUT_PREP.sh .
ln -s $RUNDIR/SCRIPTS/ADD_DATE.sh .
ln -s $RUNDIR/SCRIPTS/EXTRACT_MEANSTD.EXE .
ln -s $RUNDIR/SCRIPTS/EXTRACT_MEANSTD.py .
ln -s $RUNDIR/SCRIPTS/TRIP_ASSIM.EXE .
ln -s $RIVEXE trip_assim.exe
# set options
sed -i "s/#SBATCH -n .*/#SBATCH -n $((NENS+1))/" TRIP_ASSIM.EXE
shopt -s nullglob

#============================================
# loop over months
#============================================
echo "***************************************"
echo "   READY TO START SIMULATION"
echo "       EXP: $EXP"
echo "       $YYYYMM_beg  ->  $YYYYMM_end"
echo "***************************************"
date > date_$EXP
YYYYMM=$YYYYMM_beg
TMAX=10800
while [ $YYYYMM -le $YYYYMM_end ]; do

  # get forcing for this month
  ./GET_FORCING.sh -r -t ${FORCINGTARNAME}_${YYYYMM::4}.tar -n $FORCINGNAME -e _001 $YYYYMM $SCRATCHDIR/RUN_ISBA $FORCINGDIR
  YYYYMM_next_year=`./ADD_DATE.sh $YYYYMM 1year`
  [ $YYYYMM_next_year -le $YYYYMM_end ] && \
    ./GET_FORCING.sh -t ${FORCINGTARNAME}_${YYYYMM_next_year::4}.tar -n $FORCINGNAME -e _001 $YYYYMM_next_year $SCRATCHDIR/RUN_ISBA $FORCINGDIR &
  IENS=1
  while [ $IENS -le $NENS ] ; do
    IENS0=`printf %03d $IENS`
    ln -sf $SCRATCHDIR/RUN_ISBA/${FORCINGNAME}_${YYYYMM}_${IENS0}.nc TRIP_FORCING_${IENS0}.nc
    IENS=$((IENS+1))
  done

  # get observations
  if [ $OPENLOOP -eq 0 ] ; then
    ln -sf $OBSDIR/OBS_${YYYYMM}.nc OBS.nc
  else
    cp $OBSDIR/OBS_empty.nc OBS.nc
    ncatted -a units,time,m,c,"days since ${YYYYMM::4}-${YYYYMM:4:2}-01 00:00:00" OBS.nc
  fi

  sleep 1
  [ ${YYYYMM:4:2} -eq "01" ] && echo "--------------------"

  echo $YYYYMM
  YYYYMM_next=`./ADD_DATE.sh $YYYYMM 1month`

  # run trip assimilation
  rm -f finished
  sbatch ./TRIP_ASSIM.EXE $NENS $YYYYMM $SCRATCHDIR/$EXP &>/dev/null
  T=0 ; while [ ! -f finished ] && [ $T -lt $TMAX ]; do T=$((T+1)); sleep 1; done
  TRIP_RESTART=( TRIP_RESTART* )
  [ ${#TRIP_RESTART[@]} -ne $NENS ] && echo "Error with TRIP_ASSIM.EXE ${YYYYMM}" && exit 1

  # TRIP_RESTART file is TRIP_PREP file for next month
  IENS=1
  while [ $IENS -le $NENS ] ; do
    IENS0=`printf %03d $IENS`
    mv -f TRIP_RESTART_${IENS0}.nc TRIP_PREP_${IENS0}.nc
    [ ${YYYYMM_next:4:2} -eq 01 ] && cp TRIP_PREP_${IENS0}.nc $SCRATCHDIR/$EXP/TRIP_PREP/TRIP_PREP_${YYYYMM_next}${DDHH}_${IENS0}.nc
    IENS=$((IENS+1))
  done

  # compute ensemble mean/std
  rm -f extract_finished.$YYYYMM
  sbatch ./EXTRACT_MEANSTD.EXE $YYYYMM $SCRATCHDIR/$EXP $NENS &>/dev/null

  # save results (only first month of year)
  [ ${YYYYMM_next:4:2} -eq 01 ] && ./PUT_RESULT.sh -r -d ENS $YYYYMM $SCRATCHDIR/$EXP $RESULTSDIR &

  YYYYMM=$YYYYMM_next
  [ ${YYYYMM:4:2} -eq 01 ] && [ $YYYYMM -le $YYYYMM_end ] && echo "-------------"

done

# save prep and results for last year if not complete
./PUT_PREP.sh $SCRATCHDIR/$EXP/TRIP_PREP $RESULTSDIR &
[ ${YYYYMM_next:4:2} -ne 01 ] && YYYYMM=`./ADD_DATE.sh $YYYYMM -1month` ./PUT_RESULT.sh -r -d ENS $YYYYMM $SCRATCHDIR/$EXP $RESULTSDIR &

# end simulation
date >> date_$EXP
echo "***************************************"
echo "   SIMULATION ENDS CORRECTLY"
echo "***************************************"
