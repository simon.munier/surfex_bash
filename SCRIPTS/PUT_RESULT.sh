#!/bin/bash
#=========================================================================
#
# Upload ISBA / CTRIP output files to hendrix for the current year
#
# USAGE
#  ./PUT_RESULT.sh [-r] [-d ENSDIR] YYYYMM SCRATCHDIR RESDIR
#
# Arguments
#   YYYYMM       : year and month of simulation
#   SCRATCHDIR   : directory where results are stored during the run
#   RESDIR       : storing directory on hendrix
#
# Options
#   -r           : remove files after transfer on hendrix
#   -d           : ensemble directory for assimilation case
#
#=========================================================================
#
# Options
RM_FILES=0
ENSDIR=""
while getopts "rd:" OPT ; do
  case $OPT in
    r)  RM_FILES=1
        ;;
    d)  ENSDIR=$OPTARG
        ;;
    \?) echo "$OPTARG: invalid option"
        ;;
  esac
done
shift $((OPTIND-1))
if [ $# -lt 3 ] ; then
  echo "PUT_RESULT error: not enough arguments"
  exit 1
fi
#
# Arguments
YYYYMM=$1
SCRATCHDIR=$2
RESDIR=$3
#
YEAR=${YYYYMM::4}
#
#=========================================================================
#
# If ensemble, wait if ensemble mean extraction is not finished
if [ ! -z $ENSDIR ] ; then
  TMAX=10800
  T=0 ; while [ ! -f extract_finished.$YYYYMM ] && [ $T -lt $TMAX ]; do T=$((T+1)); sleep 1; done
fi
#
#echo "-------------"
#echo " Saving $YEAR"
#echo "-------------"
#
# Copy options file
[ -f OPTIONS.nam ] && cp -f OPTIONS.nam $SCRATCHDIR/$ENSDIR
[ -f TRIP_OPTIONS.nam ] && cp -f TRIP_OPTIONS.nam $SCRATCHDIR/$ENSDIR
#
cat > ftpput.$YEAR << EOF
#!/bin/bash
#SBATCH -J ftpput.$YEAR
#SBATCH -p transfert
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -t 01:00:00
#SBATCH -o log_ftpput.$YEAR
#SBATCH -e log_ftpput.$YEAR
#
shopt -s nullglob
cd $SCRATCHDIR/$ENSDIR
#
# ISBA DIAG files
ISBA_FILES=( ISBA_DIAG*.nc )
if [ \${#ISBA_FILES[@]} -gt 0 ] ; then
  tar cf ISBA_${YEAR}.tar OPTIONS.nam ISBA_DIAG*${YEAR}*.nc
  ftput -h hendrix -u $USER -o mkdir ISBA_${YEAR}.tar $RESDIR/ISBA_${YEAR}.tar
  rm -f ISBA_${YEAR}.tar
fi
#
# TRIP DIAG files
TRIP_FILES=( TRIP_DIAG*.nc )
if [ \${#TRIP_FILES[@]} -gt 0 ] ; then
  tar cf TRIP_${YEAR}.tar TRIP_OPTIONS.nam TRIP_DIAG_${YEAR}*.nc
  ftput -h hendrix -u $USER -o mkdir TRIP_${YEAR}.tar $RESDIR/TRIP_${YEAR}.tar
  rm -f TRIP_${YEAR}.tar
fi
#
EOF
#
chmod u+x ftpput.${YEAR}
job_command='sbatch'
if [ $HOSTNAME == "sxsurf" ] ; then
  sed -i 's/transfert/PARTAGEE\n#SBATCH --oversubscribe/;/-t/d' ftpput.${YEAR}
  job_command='sqsub'
fi
jobid_ftp=`$job_command ftpput.${YEAR}`
jobid_ftp=`echo $jobid_ftp | awk '{print $4}'`
DT=2; while [ ! -z "`squeue --job $jobid_ftp 2> /dev/null | grep $jobid_ftp`" ]; do sleep $DT; done
rm -f ftpput.${YEAR} log_ftpput.${YEAR} extract_finished.$YYYYMM
#
[ $RM_FILES -eq 1 ] && find $SCRATCHDIR -name "ISBA_DIAG*${YEAR}*.nc" -delete \
                    && find $SCRATCHDIR -name "TRIP_DIAG_${YEAR}*.nc" -delete
#
exit 0
