#=========================================================================
#
# Script to check consistency between dates from FORCING, PREP and TRIP_PREP
#
# USAGE
#   ./CHECK_DATES.sh
#
#=========================================================================
#
LOGFILE=check_dates.log
rm -f $LOGFILE
#
DATES_OK=1
#
# FORCING.nc
if [ ! -f FORCING.nc ] ; then
  TFRC="FORCING.nc not found"
  DATES_OK=0
else
  DDD=`ncdump -h FORCING.nc | grep 'time:units' | sed -r 's/.*hours since (.*)" ;/\1/;s/[-:]/ /g'`
  START=`ncdump -v time FORCING.nc | sed '1,/data:/d' | sed -n '/time/p;s/,//' | awk '{print $3*3600}'`
  TFRC=`echo $DDD | awk -v START=$START '{print $1,$2,$3,$4*3600+$5*60+$6+START}'`
  TFRC=`echo $TFRC | awk '{print 1*$1,1*$2,1*$3,1*$4}'`
fi
echo "FORCING     : $TFRC" > $LOGFILE
#
# PREP.nc
if [ ! -f PREP.nc ] ; then
  TSFX="PREP.nc not found"
  DATES_OK=0
else
  TSFX=`ncdump -v DTCUR-YEAR PREP.nc | sed -e '1,/data:/d' | sed -n '/=/{s/.* =//;s/ ;//;p}' | sed ':a;N;$!ba;s/\n/,/g'`
  TSFX=${TSFX}`ncdump -v DTCUR-MONTH PREP.nc | sed -e '1,/data:/d' | sed -n '/=/{s/.* =//;s/ ;//;p}' | sed ':a;N;$!ba;s/\n/,/g'`
  TSFX=${TSFX}`ncdump -v DTCUR-DAY PREP.nc | sed -e '1,/data:/d' | sed -n '/=/{s/.* =//;s/ ;//;p}' | sed ':a;N;$!ba;s/\n/,/g'`
  TSFX=${TSFX}`ncdump -v DTCUR-TIME PREP.nc | sed -e '1,/data:/d' | sed -n '/=/{s/.* =//;s/ ;//;p}' | sed ':a;N;$!ba;s/\n/,/g'`
  TSFX=`echo ${TSFX} | sed -e 's/^ *//'`
  #TSFX=`echo $TSFX | awk '{print 1*$1,1*$2,1*$3,1*$4}'`
fi
echo "SURFEX PREP : $TSFX" >> $LOGFILE
[ "$TSFX" != "$TFRC" ] && DATES_OK=0 && echo "Problem with SURFEX PREP date (see $LOGFILE)"
#
# TRIP_PREP.nc
if [ -f TRIP_PREP.nc ]; then
  TTRP=`ncdump -v date TRIP_PREP.nc | sed -e '1,/data:/d' -e '1,2d' -e '$d' -e '{s/date = //;s/ ;//}'`
  TTRP=`echo $TTRP | awk '{print 1*$1,1*$2,1*$3,1*$4}'`
  echo "TRIP PREP   : $TTRP" >> $LOGFILE
  [ "$TTRP" != "$TFRC" ] && DATES_OK=0 && echo "Problem with TRIP PREP date (see $LOGFILE)"
fi
#
[ "$DATES_OK" == 1 ] && echo "Dates OK" >> $LOGFILE && exit 0 || exit 1
