#!/bin/bash
#=========================================================================
#
# Upload ISBA / CTRIP prep files to hendrix
#
# USAGE
#  ./PUT_PREP.sh [-r] SCRATCHDIR RESDIR
#
# Arguments
#   SCRATCHDIR   : directory where results are stored during the run
#   RESDIR       : storing directory on hendrix
#
# Options
#   -r           : remove files after transfer on hendrix
#
#=========================================================================
#
# Options
RM_FILES=0
while getopts "r" OPT ; do
  case $OPT in
    r)  RM_FILES=1
        ;;
    \?) echo "$OPTARG: invalid option"
        ;;
  esac
done
shift $((OPTIND-1))
if [ $# -lt 2 ] ; then
  echo "PUT_PREP error: not enough arguments"
  exit 1
fi
#
# Arguments
SCRATCHDIR=$1
RESDIR=$2
#
#=========================================================================
#
# Copy options file
[ -f OPTIONS.nam ] && cp -f OPTIONS.nam $SCRATCHDIR
[ -f TRIP_OPTIONS.nam ] && cp -f TRIP_OPTIONS.nam $SCRATCHDIR
#
cat > ftpput.prep << EOF
#!/bin/bash
#SBATCH -J ftpput.prep
#SBATCH -p transfert
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -t 01:00:00
#SBATCH -o log_ftpput.prep
#SBATCH -e log_ftpput.prep
#
shopt -s nullglob
cd $SCRATCHDIR
#
# ISBA PREP files
ISBA_FILES=( PREP*.nc )
if [ \${#ISBA_FILES[@]} -gt 0 ] ; then
  tar cf ISBA_PREP.tar OPTIONS.nam PREP*.nc
  ftput -h hendrix -u $USER -o mkdir ISBA_PREP.tar $RESDIR/ISBA_PREP.tar
  rm ISBA_PREP.tar
fi
#
# TRIP PREP files
TRIP_FILES=( TRIP_PREP*.nc )
if [ \${#TRIP_FILES[@]} -gt 0 ] ; then
  tar cf TRIP_PREP.tar TRIP_OPTIONS.nam TRIP_PREP*.nc
  ftput -h hendrix -u $USER -o mkdir TRIP_PREP.tar $RESDIR/TRIP_PREP.tar
  rm TRIP_PREP.tar
fi
#
EOF
#
chmod u+x ftpput.prep
job_command='sbatch'
if [ $HOSTNAME == "sxsurf" ] ; then
  sed -i 's/transfert/PARTAGEE\n#SBATCH --oversubscribe/;/-t/d' ftpput.prep
  job_command='sqsub'
fi
jobid_ftp=`$job_command ftpput.prep`
jobid_ftp=`echo $jobid_ftp | awk '{print $4}'`
DT=2; while [ ! -z "`squeue --job $jobid_ftp 2> /dev/null | grep $jobid_ftp`" ]; do sleep $DT; done
rm -f ftpput.prep log_ftpput.prep
#
[ $RM_FILES -eq 1 ] && rm -f $SCRATCHDIR/PREP* $SCRATCHDIR/TRIP_PREP*
#
