#!/bin/bash
#=========================================================================
#
# Download forcing files from hendrix for the current year
#
# USAGE
#  ./GET_FORCING.sh [options] FORC_DATE SCRATCHDIR FORC_DIR
#
# Arguments
#   FORC_DATE    : forcing date (YYYYMM)
#   SCRATCHDIR   : directory where forcings are stored temporally
#   FORC_DIR     : forcing directory on hendrix
#
# Options
#   -r           : remove forcing files from previous year
#   -t TAR_NAME  : if forcing files are tared on hendrix (default no)
#   -n FORC_NAME : baseline of forcing file name (default=FORCING)
#   -e FORC_EXT  : extension in forcing file name (default="")
#
# The netCDF forcing file has the form:
#   ${FORC_NAME}_${FORC_DATE}${FORC_EXT}.nc
#
# The tar forcing file includes all the monthly netCDF files for the current year.
#
#=========================================================================
#
# Options
RM_OLD=0
TAR_NAME=""
FORC_NAME="FORCING"
FORC_EXT=""
while getopts "rt:n:e:" OPT ; do
  case $OPT in
    r)  RM_OLD=1
        ;;
    t)  TAR_NAME=$OPTARG
        ;;
    n)  FORC_NAME=$OPTARG
        ;;
    e)  FORC_EXT=$OPTARG
        ;;
    :)  echo "The option $OPTARG requires an argument"
        exit 1
        ;;
    \?) echo "$OPTARG: invalid option"
        ;;
  esac
done
shift $((OPTIND-1))
if [ $# -lt 3 ] ; then
  echo "GET_FORCING error: not enough arguments"
  exit 1
fi
#
# Arguments
FORC_DATE=$1
SCRATCHDIR=$2
FORC_DIR=$3
#
RUNDIR=$(pwd)
YEAR=${FORC_DATE::4}
#
#=========================================================================
#
# check SCRATCHDIR
if [ ${SCRATCHDIR::9} != "/scratch/" ] && [ ${SCRATCHDIR::6} != "/cnrm/" ] ; then
  echo "Forcings should be downloaded in /scratch/work/$(whoami)"
  echo "Please check SCRATCHDIR:"
  echo "$SCRATCHDIR"
  exit 1
fi
#
mkdir -p $SCRATCHDIR
#
# remove files for previous years
[ $RM_OLD -eq 1 ] && find $SCRATCHDIR -name "*$((YEAR-1))*" -delete
#
# check if file already exists for this month or if first month of year
[ -f $SCRATCHDIR/${FORC_NAME}_${FORC_DATE}${FORC_EXT}.nc ] && exit 0
#
# wait if transfer is already under process
if [ -f log_ftpget.${YEAR} ] ; then
  while [ -f log_ftpget.${YEAR} ] ; do sleep 1 ; done
  exit 0
fi
#
# list of forcing files to get from hendrix
rm -f forclist.${YEAR}${FORC_EXT}
touch forclist.${YEAR}${FORC_EXT}
if [ -z $TAR_NAME ] ; then
  for MONTH in `seq ${FORC_DATE:4:2} 12` ; do
    MONTH0=`printf %02d $MONTH`
    [ -f $SCRATCHDIR/${FORC_NAME}_${YEAR}${MONTH0}${FORC_EXT}.nc ] && continue
    echo "${FORC_DIR}/${FORC_NAME}_${YEAR}${MONTH0}${FORC_EXT}.nc $SCRATCHDIR/${FORC_NAME}_${YEAR}${MONTH0}${FORC_EXT}.nc" >> forclist.${YEAR}${FORC_EXT}
  done
else
  echo ${FORC_DIR}/${TAR_NAME} $SCRATCHDIR/${TAR_NAME} >> forclist.${YEAR}${FORC_EXT}
fi
#
[ ! -s forclist.${YEAR}${FORC_EXT} ] && exit 0
#
cat > ftpget.${YEAR}${FORC_EXT} << EOF
#!/bin/bash
#SBATCH -J ftpget.${YEAR}${FORC_EXT}
#SBATCH -p transfert
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -t 01:00:00
#SBATCH -o log_ftpget.${YEAR}${FORC_EXT}
#SBATCH -e log_ftpget.${YEAR}${FORC_EXT}
cd $RUNDIR
[ $HOSTNAME == "sxsurf" ] && ftmotpasse
ftget -h hendrix -u $USER < forclist.${YEAR}${FORC_EXT}
EOF
#
echo  "Getting forcing for year $YEAR"
#
chmod u+x ftpget.${YEAR}${FORC_EXT}
job_command='sbatch'
if [ $HOSTNAME == "sxsurf" ] ; then
  sed -i 's/transfert/PARTAGEE/;/-t/d' ftpget.${YEAR}${FORC_EXT}
  sed -i 's/</-i/' ftpget.${YEAR}${FORC_EXT}
  job_command='sqsub'
fi
jobid_ftp=`$job_command ftpget.${YEAR}${FORC_EXT}`
jobid_ftp=`echo $jobid_ftp | awk '{print $4}'`
DT=2; while [ ! -z "`squeue --job $jobid_ftp 2> /dev/null | grep $jobid_ftp`" ]; do sleep $DT; done
rm -f ftpget.${YEAR}${FORC_EXT} log_ftpget.${YEAR}${FORC_EXT} forclist.${YEAR}${FORC_EXT}
#
# untar file
[ ! -z $TAR_NAME ] && cd $SCRATCHDIR && tar xf $TAR_NAME && rm $TAR_NAME
#
exit 0
