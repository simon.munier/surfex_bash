#!/usr/bin/python
from netCDF4 import Dataset
import numpy as np
import re
from sys import argv


def create_oasisRestart(lonlat,varlist,filename,lonlatval=False):
  '''
  Create oasis restart files (lsm.nc or trip.nc) given longitude and latitude
  '''
  
  lon,lat = lonlat
  
  # Variables attributes
  var = {'SXRUNOFF':{'name':'Surface Runoff','units':'kg/m2'}, \
         'SXDRAIN' :{'name':'Deep Drainage','units':'kg/m2'}, \
         'SXGWRECH':{'name':'Groundwater recharge','units':'kg/m2'}, \
         'SXSRCFLD':{'name':'Floodplains water budget','units':'kg/m2'}, \
         'TRFWTD'  :{'name':'Grid-cell fraction of WTD to rise','units':'-'}, \
         'TRWTD'   :{'name':'Water table depth','units':'m'}, \
         'TRFFLD'  :{'name':'Floodplain fraction','units':'-'}, \
         'TRPIFLD' :{'name':'Flood potential infiltration','units':'kg/m2/s'} \
        }

  # Create netcdf file
  nc = Dataset(filename,'w')

  # Dimensions
  if not lonlatval:
    nc.createDimension('longitude',len(lon))
    nc.createDimension('latitude',len(lat))
  else:
    nc.createDimension('Number_of_points',len(lon))
    nc.createDimension('col',len(lat))

  # Dimension variables
  if not lonlatval:
    nc.createVariable('longitude','f4',('longitude',))
    nc.variables['longitude'].setncatts({ \
      'standard_name':'longitude', \
      'long_name':'longitude', \
      'units':'degrees_east', \
      'axis':'X'})
    nc.variables['longitude'][:] = lon
    nc.createVariable('latitude','f4',('latitude',))
    nc.variables['latitude'].setncatts({ \
      'standard_name':'latitude', \
      'long_name':'latitude', \
      'units':'degrees_north', \
      'axis':'Y'})
    nc.variables['latitude'][:] = lat

  # Gridded variables
  for v in varlist:
    if not lonlatval:
      nc.createVariable(v,'f4',('latitude','longitude'))
    else:
      nc.createVariable(v,'f4',('Number_of_points','col'))
    nc.variables[v].setncatts({ \
      'long_name':var[v]['name'], \
      'units':var[v]['units'], \
      '_FillValue':np.array(1e20,'f4'), \
      'missing_value':np.array(1e20,'f4')})
    nc.variables[v][:] = np.zeros((len(lat),len(lon)))

  # Close netcdf file
  nc.close()


def get_lonlat(namlist):
  '''
  '''
  zone = [[None,None],[None,None]]
  res = None
  lonlatval = False
  with open(namlist) as fid:
    for line in fid:
      if   line.find('LONLATVAL')>=0:
        lonlatval = True
      if   line.find('NPOINTS')>=0:
        Number_of_points = int(re.search('NPOINTS *= *(.[0-9]*) *',line).group(1))
      elif line.find('LONMIN')>=0:
        zone[0][0] = float(re.search('LONMIN *= *(.[0-9]*.*[0-9*]) *',line).group(1))
      elif line.find('LONMAX')>=0:
        zone[1][0] = float(re.search('LONMAX *= *(.[0-9]*.*[0-9*]) *',line).group(1))
      elif line.find('LATMIN')>=0:
        zone[1][1] = float(re.search('LATMIN *= *(.[0-9]*.*[0-9*]) *',line).group(1))
      elif line.find('LATMAX')>=0:
        zone[0][1] = float(re.search('LATMAX *= *(.[0-9]*.*[0-9*]) *',line).group(1))
      elif line.find(' NLON')>=0:
        nlon = int(re.search('NLON *= *(.[0-9]*) *',line).group(1))
      elif line.find(' NLAT')>=0:
        nlat = int(re.search('NLAT *= *(.[0-9]*) *',line).group(1))
      elif line.find(' TRES')>=0:
        res = float(re.search('TRES *= *(.[0-9]*.*[0-9*]) *',line).group(1))
  if lonlatval:
    lon = np.arange(Number_of_points)
    lat = np.arange(1)
  else:
    if any([v is None for w in zone for v in w]):
      raise Exception('Problem in bounding box definition for {0}'.format(namlist))
    if res is not None:
      reslon,reslat = res,res
    else:
      reslon = (zone[1][0]-zone[0][0])/nlon
      reslat = (zone[0][1]-zone[1][1])/nlat
    lon = np.arange(zone[0][0]+reslon/2,zone[1][0],reslon);
    lat = np.arange(zone[1][1]+reslat/2,zone[0][1],reslat);
    #lat = np.arange(zone[0][1]-reslat/2,zone[1][1],-reslat);
  return lon,lat,lonlatval


if __name__ == '__main__':
  
  SAVEDIR = argv[1]
  PREPDAT = argv[2]
  
  lon_sfx,lat_sfx,lonlatval = get_lonlat('OPTIONS.nam')
  lon_trp,lat_trp,_ = get_lonlat('TRIP_OPTIONS.nam')

  modvar = None
  sfxlist,trplist = [],[]
  with open('namcouple') as fid:
    for line in fid:
      if line[0] == '#': continue
      if modvar == 'sfx' and not [int(n) for n in re.findall('([0-9]*) *',line)[:4]] == \
          [len(lon_sfx),len(lat_sfx),len(lon_trp),len(lat_trp)]:
        raise Exception('Grid size inconsistency in namcouple file for variable {0}'.format(sfxlist[-1]))
      elif modvar == 'trp' and not [int(n) for n in re.findall('([0-9]*) *',line)[:4]] == \
          [len(lon_trp),len(lat_trp),len(lon_sfx),len(lat_sfx)]:
        raise Exception('Grid size inconsistency in namcouple file for variable {0}'.format(trplist[-1]))
      modvar = None
      if line.find('EXPORTED')>=0:
        var = re.search('([^ ]*) *',line).group(1)
        if var in ('SXRUNOFF','SXDRAIN','SXGWRECH','SXSRCFLD'):
          sfxlist.append(var)
          modvar = 'sfx'
        elif var in ('TRFWTD','TRWTD','TRFFLD','TRPIFLD'):
          trplist.append(var)
          modvar = 'trp'

  if len(sfxlist)>0: print('lsm.nc  : '+' / '.join(sfxlist))
  if len(trplist)>0: print('trip.nc : '+' / '.join(trplist))

  create_oasisRestart((lon_sfx,lat_sfx),sfxlist,SAVEDIR+'/lsm_'+PREPDAT+'.nc',lonlatval)
  create_oasisRestart((lon_trp,lat_trp),trplist,SAVEDIR+'/trip_'+PREPDAT+'.nc')

