import os,sys
import numpy as np
from netCDF4 import Dataset

YYYYMM = sys.argv[1]
run_dir = sys.argv[2]
N = int(sys.argv[3])

print('='*len(run_dir+' - '+YYYYMM))
print(run_dir+' - '+YYYYMM)
print('='*len(run_dir+' - '+YYYYMM))

if run_dir[-1] != '/': run_dir = run_dir+'/'

ens_dir = run_dir+'ENS/'

if not os.path.isdir(ens_dir): os.mkdir(ens_dir)
file_format = run_dir+'{0:03d}/TRIP_DIAG_'+YYYYMM+'_{0:03d}.nc'

with Dataset(file_format.format(1)) as nc:
  lon,lat = nc['longitude'][:],nc['latitude'][:]
  time,time_units = nc['time'][:],nc['time'].units
  var = [v for v in nc.variables.keys() if v not in ['longitude','latitude','time']]

Vmoy,Vstd = {},{}
Vens = np.zeros((N,len(time),len(lat),len(lon)),'f4')
for v in var:
  for i in range(N):
    with Dataset(file_format.format(i+1)) as nc:
      Vens[i] = nc[v][:].filled(1e20)
  mask = (Vens[0]==1e20)
  Vmoy[v] = np.where(mask,np.nan,np.nanmean(Vens,axis=0))
  Vstd[v] = np.where(mask,np.nan,np.nanstd(Vens,axis=0))

print(ens_dir+'TRIP_DIAG_'+YYYYMM+'.nc')
with Dataset(ens_dir+'TRIP_DIAG_'+YYYYMM+'.nc','w') as nc:
  nc.createDimension('longitude',len(lon))
  nc.createDimension('latitude',len(lat))
  nc.createDimension('time',None)
  nc.createVariable('longitude','f4','longitude')
  nc['longitude'][:] = lon
  nc.createVariable('latitude','f4','latitude')
  nc['latitude'][:] = lat
  nc.createVariable('time','f4','time')
  nc['time'].setncatts({'units':time_units})
  nc['time'][:] = time
  for v in var:
    nc.createVariable(v,'f4',('time','latitude','longitude'),fill_value=1e20)
    nc[v][:] = Vmoy[v]
    nc.createVariable(v+'_std','f4',('time','latitude','longitude'),fill_value=1e20)
    nc[v+'_std'][:] = Vstd[v]

