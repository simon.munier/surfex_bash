#!/bin/bash
if [ $# -ne 2 ]; then
  echo "Usage: $0 DATE INC"
  echo "  DATE must have one of the following forms"
  echo "      YYYY"
  echo "      YYYYMM"
  echo "      YYYYMMDD"
  echo "  INC can be +/-X years/months/days/hours, X is an integer"
  echo "  Output has the same form"
  exit
fi
YMDH=$1
INC=$2
N=${#YMDH}
[ ${#YMDH} -eq 4 ] && YMDH=${YMDH}01
[ ${#YMDH} -eq 6 ] && YMDH=${YMDH}01
[ ${#YMDH} -eq 8 ] && YMDH=${YMDH}00
YMDH=`date +%Y%m%d%H -d "${YMDH::8} ${YMDH:8:2}:00:00 UTC ${INC}"`
echo "${YMDH::N}"
