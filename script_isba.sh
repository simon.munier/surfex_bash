#!/bin/bash
#=========================================================================
#
# Script to run SURFEX with forcings at a monthly time step
#
# USAGE
#   ./script_isba.sh EXP
#
# by default: EXP=sfx
#
#=========================================================================
#set -x

REGION=Spain
EXP=${1:-sfx}

SURFEXNAML=OPTIONS.nam
TRIPNAML=TRIP_OPTIONS.nam
OASISNAML=namcouple

#============================================
# simulation time span
#============================================
YYYYMM_beg=200901
YYYYMM_end=200912
DDHH=0100

#============================================
# check if TRIP is used
#============================================
LTRIP=`grep "LOASIS *= *T" OPTIONS/$SURFEXNAML | wc -l`

#============================================
# directories and links
#============================================
VER=LXifort-SFX-V9-0-MPIAUTO-OMP-O2-X0
TRUNKDIR=$HOME/src/SURFEX_V9_DEV/Surfex_Git2

SFXEXE=$TRUNKDIR/exe/OFFLINE-$VER
[ $LTRIP -eq 1 ] && RIVEXE=$TRUNKDIR/exe/TRIP_MASTER-$VER
ECODIR=$TRUNKDIR/MY_RUN/ECOCLIMAP

SCRATCHDIR=$WORKDIR/SURFEX/$REGION
FORCINGDIR=/home/$(whoami)/SURFEX/$REGION/forcings #on Hendrix
RESULTSDIR=/home/$(whoami)/SURFEX/$REGION/$EXP     #on Hendrix

#============================================
# create directories and copy files
#============================================
RUNDIR=$(pwd)
mkdir -p $SCRATCHDIR/$EXP
rm -rf $SCRATCHDIR/RUN/$EXP
mkdir -p $SCRATCHDIR/RUN/$EXP
cd $SCRATCHDIR/RUN/$EXP

# get params
ln -s $ECODIR/ecoclimapI_covers_param.bin .
ln -s $ECODIR/ecoclimapII_eu_covers_param.bin .
ln -s $RUNDIR/PGD_PREP/PGD.nc .
[ $LTRIP -eq 1 ] && ln -s $RUNDIR/PGD_PREP/TRIP_PARAM.nc .
[ $LTRIP -eq 1 ] && ln -s $RUNDIR/PGD_PREP/areas.nc .
[ $LTRIP -eq 1 ] && ln -s $RUNDIR/PGD_PREP/grids.nc .
[ $LTRIP -eq 1 ] && ln -s $RUNDIR/PGD_PREP/masks.nc .
OASISDIR=$RUNDIR/data_oasis
LRMP=`( [ -d $OASISDIR ] && find $OASISDIR -name rmp*.nc ) | wc -l`
[ $LTRIP -eq 1 ] && [ $LRMP -gt 0 ] && ln -s $OASISDIR/rmp*.nc .
# get restart
ln -s $RUNDIR/PGD_PREP/PREP_${YYYYMM_beg}${DDHH}.nc PREP.nc
[ $LTRIP -eq 1 ] && ln -s $RUNDIR/PGD_PREP/TRIP_PREP_${YYYYMM_beg}${DDHH}.nc TRIP_PREP.nc
[ $LTRIP -eq 1 ] && cp -f $RUNDIR/PGD_PREP/lsm_${YYYYMM_beg}${DDHH}.nc lsm.nc
[ $LTRIP -eq 1 ] && cp -f $RUNDIR/PGD_PREP/trip_${YYYYMM_beg}${DDHH}.nc trip.nc
( [ ! -f PREP.nc ] || ( [ $LTRIP -eq 1 ] && ( [ ! -f TRIP_PREP.nc ] || [ ! -f lsm.nc ] ) ) ) && \
  echo "Some PREP files are missing" && exit 1
# get option files
cat $RUNDIR/OPTIONS/CSELECT.nam $RUNDIR/OPTIONS/$SURFEXNAML > OPTIONS.nam
[ $LTRIP -eq 1 ] && cp -f $RUNDIR/OPTIONS/$TRIPNAML TRIP_OPTIONS.nam
[ $LTRIP -eq 1 ] && cp -f $RUNDIR/OPTIONS/$OASISNAML namcouple_xxx
# get executables
ln -s $RUNDIR/SCRIPTS/CHECK_DATES.sh .
ln -s $RUNDIR/SCRIPTS/ADD_DATE.sh .
ln -s $RUNDIR/SCRIPTS/GET_FORCING.sh .
ln -s $RUNDIR/SCRIPTS/PUT_RESULT.sh .
ln -s $RUNDIR/SCRIPTS/PUT_PREP.sh .
ln -s $RUNDIR/SCRIPTS/ISBA_OFFLINE.EXE .
ln -s $SFXEXE offline.exe
[ $LTRIP -eq 1 ] && ln -s $RIVEXE trip.exe

#============================================
# loop over months
#============================================
echo "***************************************"
echo "   READY TO START SIMULATION"
echo "       EXP: $EXP"
echo "       $YYYYMM_beg  ->  $YYYYMM_end"
echo "***************************************"
date > date_$EXP
YYYYMM=$YYYYMM_beg
TMAX=`grep '#SBATCH -t' ISBA_OFFLINE.EXE | awk '{print $3}' | awk -F ':' '{print 5+3600*$1+60*$2+$3}'`
while [ $YYYYMM -le $YYYYMM_end ]; do

  # get forcing for this month
  ./GET_FORCING.sh -r $YYYYMM $SCRATCHDIR/FORCING_ISBA $FORCINGDIR
  ln -sf $SCRATCHDIR/FORCING_ISBA/FORCING_${YYYYMM}.nc FORCING.nc
  YYYYMM_next_year=`./ADD_DATE.sh $YYYYMM 1year`
  [ $YYYYMM_next_year -le $YYYYMM_end ] && \
    ./GET_FORCING.sh $YYYYMM_next_year $SCRATCHDIR/FORCING_ISBA $FORCINGDIR &
  ./CHECK_DATES.sh || exit 1

  sleep 1
  [ ${YYYYMM:4:2} -eq "01" ] && echo "--------------------"

  echo $YYYYMM
  YYYYMM_next=`./ADD_DATE.sh $YYYYMM 1month`

  # adjust run length in namcouple
  TEND=`ncdump -v time FORCING.nc | sed -n '1,/data:/d;p' | sed ':a;N;$!ba;s/\n/ /g' | awk '{print 3600*($(NF-2)-$3)}'`
  TEND=$((TEND-10800))
  [ $LTRIP -eq 1 ] && sed -e "s/XXXXXX/$TEND/" namcouple_xxx >  namcouple

  # run OFFLINE
  rm -f finished
  sbatch ./ISBA_OFFLINE.EXE $YYYYMM $SCRATCHDIR/$EXP &>/dev/null
  T=0 ; while [ ! -f finished ] && [ $T -lt $TMAX ]; do T=$((T+1)); sleep 1; done
  [ ! -f SURFOUT.nc ] && echo "Error with offline ${YYYYMM}" && exit 1

  # SURFOUT file is PREP file for next month
  mv -f SURFOUT.nc PREP.nc
  [ $LTRIP -eq 1 ] && mv -f TRIP_RESTART.nc TRIP_PREP.nc
  [ ${YYYYMM_next:4:2} -eq 01 ] && cp PREP.nc $SCRATCHDIR/$EXP/PREP_${YYYYMM_next}${DDHH}.nc && \
    [ $LTRIP -eq 1 ] && cp TRIP_PREP.nc $SCRATCHDIR/$EXP/TRIP_PREP_${YYYYMM_next}${DDHH}.nc

  # save results (only first month of year)
  [ ${YYYYMM_next:4:2} -eq 01 ] && ./PUT_RESULT.sh $YYYYMM $SCRATCHDIR/$EXP $RESULTSDIR &

  YYYYMM=$YYYYMM_next

done

# save prep and results for last year if not complete
./PUT_PREP.sh $SCRATCHDIR/$EXP $RESULTSDIR &
[ ${YYYYMM_next:4:2} -ne 01 ] && YYYYMM=`./ADD_DATE.sh $YYYYMM -1month` && ./PUT_RESULT.sh $YYYYMM $SCRATCHDIR/$EXP $RESULTSDIR

# save rmp files for next run
[ $LTRIP -eq 1 ] && [ $LRMP -eq 0 ] && mkdir -p $OASISDIR && mv rmp*.nc $OASISDIR

# end simulation
date >> date_$EXP
echo "***************************************"
echo "   SIMULATION ENDS CORRECTLY"
echo "***************************************"
