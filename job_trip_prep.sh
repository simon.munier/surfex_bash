#!/bin/bash
#SBATCH -J trip_prep
#SBATCH -p normal256
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -t 00:30:00
#SBATCH -o trip_prep.log
#SBATCH -e trip_prep.log
#=========================================================================
#
# Script to compute the TRIP_PARAM and TRIP_PREP files
# Both files are saved in the PGD_PREP subdirectory
#
# USAGE
#   sbatch ./job_trip_prep
#
#=========================================================================
#
TRIPNAML=TRIP_OPTIONS.nam
#
# Add option suffix
OPT=""
#grep -q 'CGROUNDW.*DIF' OPTIONS/TRIP_OPTIONS.nam && GW="_gw" || GW=""
#grep -q 'LFLOOD.*T' OPTIONS/TRIP_OPTIONS.nam && FL="_fld" || FL=""
#grep -q 'CLAKE.*MLK' OPTIONS/TRIP_OPTIONS.nam && LK="_mlk" || LK=""
#OPT=$GW$FL$LK
#
#=========================================================================
# ECOCLIMAP AND SURFEX
#=========================================================================
#
VER=LXifort-SFX-V9-0-MPIAUTO-OMP-O2-X0
TRUNKDIR=$HOME/src/SURFEX_V9_DEV/Surfex_Git2
#
TRIPDIR=/home/muniers/Data/ECOCLIMAP/TRIP
#
EXETRIP=$TRUNKDIR/exe/TRIP_PREP-$VER
#
#=========================================================================
# DIRECTORIES AND INIT RUN
#=========================================================================
#
RUNDIR=$(pwd)
TMPDIR=${TMPDIR:-tmp}
PREPDIR=$RUNDIR/PGD_PREP
#
mkdir -p $PREPDIR
cd $TMPDIR
#
#=========================================================================
#
ulimit -s unlimited
export OMP_NUM_THREADS=1
export DR_HOOK=0
export DR_HOOK_OPT=prof
#
#=========================================================================
# PREP CASE
#=========================================================================
#
cp $RUNDIR/OPTIONS/$TRIPNAML TRIP_OPTIONS.nam
ln -s $TRIPDIR/TRIP_PGD_12D.nc .
ln -s $TRIPDIR/TRIP_INIT_12D.nc .
ln -s $TRIPDIR/TRIP_SGAQUI_12D.nc .
ln -s $TRIPDIR/TRIP_SGFLOOD_12D.nc .
ln -s $TRIPDIR/TRIP_LOCAL_VARIOGRAM.nc .
#
ln -s $EXETRIP trip_prep.exe
#
./trip_prep.exe
#
if [[ $? -ne 0 ]] ; then
  echo "run prep failed !!! See in directory: $TMPDIR"
  exit
fi
#
if [[ $DR_HOOK -ne 0 ]] ; then
  mv drhook.prof.1 $PREPDIR/drhook.prof_prep
  mv TRIP_PREP_LISTING.txt $PREPDIR/TRIP_PREP_LISTING.txt
fi
#
YY=`grep NYEAR TRIP_OPTIONS.nam | sed -e "s/.*= \([0-9]*\) *,/\1/"`
MM=`grep NMONTH TRIP_OPTIONS.nam | sed -e "s/.*= \([0-9]*\) *,/\1/"`
DD=`grep NDAY TRIP_OPTIONS.nam | sed -e "s/.*= \([0-9]*\) *,/\1/"`
HH=$((`grep XTIME TRIP_OPTIONS.nam | sed -e "s/.*= *\([0-9]*\).*/\1/"`/3600))
printf -v PREP_DATE "%04g%02g%02g%02g" $YY $MM $DD $HH
#
mv TRIP_PARAM.nc $PREPDIR/TRIP_PARAM${OPT}.nc
mv TRIP_PREP.nc $PREPDIR/TRIP_PREP${OPT}_${PREP_DATE}.nc
#
#=========================================================================
exit 0
#=========================================================================
