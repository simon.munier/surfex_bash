#!/bin/bash
#SBATCH -J isba_prep
#SBATCH -p normal256
#SBATCH -N 1
#SBATCH -n 128
#SBATCH -t 00:30:00
#SBATCH -o isba_prep.log
#SBATCH -e isba_prep.log
#=========================================================================
#
# Script to compute the PGD and PREP files (including oasis and trip prep files)
# All files are saved in the PGD_PREP subdirectory
#
# USAGE
#   sbatch ./job_isba_prep LPGD LPREP
#
# by default: LPGD=1 and LPREP=1
#
#=========================================================================
#
LPGD=${1:-1}
LPREP=${2:-1}
SLURM_NTASKS=${SLURM_NTASKS:-6}
#
SURFEXNAML=OPTIONS.nam
TRIPNAML=TRIP_OPTIONS.nam
OASISNAML=namcouple
#
# check if TRIP is used
LTRIP=`grep "LOASIS *= *T" OPTIONS/$SURFEXNAML | wc -l`
#
#=========================================================================
# ECOCLIMAP AND SURFEX
#=========================================================================
#
VER=LXifort-SFX-V9-0-MPIAUTO-OMP-O2-X0
TRUNKDIR=$HOME/src/SURFEX_V9_DEV/Surfex_Git2
#
ECODIR=$TRUNKDIR/MY_RUN/ECOCLIMAP
PHYSIODIR=/scratch/work/SURFEX/PHYSIO
[ $LTRIP -eq 1 ] && TRIPDIR=/home/muniers/Data/ECOCLIMAP/TRIP
#
EXEPGD=$TRUNKDIR/exe/PGD-$VER
EXEPREP=$TRUNKDIR/exe/PREP-$VER
[ $LTRIP -eq 1 ] && EXETRIP=$TRUNKDIR/exe/TRIP_PREP-$VER
#
#=========================================================================
# DIRECTORIES AND INIT RUN
#=========================================================================
#
RUNDIR=$(pwd)
TMPDIR=${TMPDIR:-tmp}
PREPDIR=$RUNDIR/PGD_PREP
#
mkdir -p $PREPDIR
cd $TMPDIR
#
#=========================================================================
#
ulimit -s unlimited
export OMP_NUM_THREADS=1
export DR_HOOK=0
export DR_HOOK_OPT=prof
#
#=========================================================================
# NAMELIST
#=========================================================================
#
cat $RUNDIR/OPTIONS/CSELECT.nam $RUNDIR/OPTIONS/$SURFEXNAML > OPTIONS.nam
[ $LTRIP -eq 1 ] && cp $RUNDIR/OPTIONS/$TRIPNAML TRIP_OPTIONS.nam
[ $LTRIP -eq 1 ] && cp $RUNDIR/OPTIONS/$OASISNAML namcouple
[ $LTRIP -eq 1 ] && sed -i "s/XXXXXX/86400/" namcouple
#
ln -s $ECODIR/ecoclimapI_covers_param.bin .
ln -s $ECODIR/ecoclimapII_eu_covers_param.bin .
#
#=========================================================================
# PGD CASE
#=========================================================================
#
if [[ $LPGD -eq 1 ]] || [[ ! -f $PREPDIR/PGD.nc ]]; then
  echo " !!!!!!!!!! PGD CASE !!!!!!!!!! "
  #
  ln -s $PHYSIODIR/TOPOGRAPHY/gtopo30.* .
  ln -s $PHYSIODIR/TOPOGRAPHY/topo_index.* .
  ln -s $PHYSIODIR/SOIL_TEXTURE/soc* .
  ln -s $PHYSIODIR/LAND_USE/perm_glo* .
  #
  ln -s $PHYSIODIR/LAND_USE/ECOCLIMAP-II/ECOCLIMAP_II_EUROP_V2.6.dir ecoclimap.dir
  ln -s $PHYSIODIR/LAND_USE/ECOCLIMAP-II/ECOCLIMAP_II_EUROP_V2.6.hdr ecoclimap.hdr
  #
  ln -s $PHYSIODIR/LAKES/LAKE_DEPTH_ECO_II_V2.3.dir lake_depth.dir
  ln -s $PHYSIODIR/LAKES/LAKE_DEPTH_ECO_II_V2.3.hdr lake_depth.hdr
  #
  ln -s $PHYSIODIR/SOIL_TEXTURE/CLAY_MOY_HWSD_v1.10_ECO_I.dir clay.dir
  ln -s $PHYSIODIR/SOIL_TEXTURE/CLAY_MOY_HWSD_v1.10_ECO_I.hdr clay.hdr
  ln -s $PHYSIODIR/SOIL_TEXTURE/SAND_MOY_HWSD_v1.10_ECO_I.dir sand.dir
  ln -s $PHYSIODIR/SOIL_TEXTURE/SAND_MOY_HWSD_v1.10_ECO_I.hdr sand.hdr
  #
  ln -s $EXEPGD pgd.exe
  #
  mpirun -np $SLURM_NTASKS ./pgd.exe
  if [[ $? -ne 0 ]] ; then
     echo "run pgd failed !!! See in directory: $TMPDIR"
     exit 1
  fi
  mv PGD.nc $PREPDIR/PGD.nc
  #
fi
#
#=========================================================================
# PREP CASE
#=========================================================================
#
if [[ $LPREP -eq 1 ]]; then
  echo " !!!!!!!!!! PREP CASE !!!!!!!!!! "
  #
  ln -s $PREPDIR/PGD.nc PGD.nc
  #
  if [ $LTRIP -eq 1 ]; then
    ln -s $TRIPDIR/TRIP_PGD_12D.nc .
    ln -s $TRIPDIR/TRIP_SGAQUI_12D.nc .
    ln -s $TRIPDIR/TRIP_SGFLOOD_12D.nc .
    ln -s $TRIPDIR/TRIP_INIT_12D.nc .
  fi
  #
  ln -s $EXEPREP prep.exe
  [ $LTRIP -eq 1 ] && ln -s $EXETRIP trip_prep.exe
  #
  if [ $LTRIP -eq 1 ]; then
    mpirun -np $((SLURM_NTASKS-1)) ./prep.exe : -np 1 ./trip_prep.exe
  else
    mpirun -np $SLURM_NTASKS ./prep.exe
  fi
  #
  if [[ $? -ne 0 ]] ; then
    echo "run prep failed !!! See in directory: $TMPDIR"
    exit
  fi
  #
  if [[ $DR_HOOK -ne 0 ]] ; then
    mv drhook.prof.1 $PREPDIR/drhook.prof_prep
    mv LISTING_PREP.txt $PREPDIR/LISTING_PREP.txt
  fi
  #
  YY=`grep NYEAR OPTIONS.nam | sed -e "s/.*= \([0-9]*\) *,/\1/"`
  MM=`grep NMONTH OPTIONS.nam | sed -e "s/.*= \([0-9]*\) *,/\1/"`
  DD=`grep NDAY OPTIONS.nam | sed -e "s/.*= \([0-9]*\) *,/\1/"`
  HH=$((`grep XTIME OPTIONS.nam | sed -e "s/.*= \([0-9]*\). *,/\1/"`/3600))
  printf -v PREP_DATE "%04g%02g%02g%02g" $YY $MM $DD $HH
  #
  mv PREP.nc $PREPDIR/PREP_${PREP_DATE}.nc
  if [ $LTRIP -eq 1 ]; then
    mv TRIP_PARAM.nc $PREPDIR/TRIP_PARAM.nc
    mv TRIP_PREP.nc $PREPDIR/TRIP_PREP_${PREP_DATE}.nc
    mv areas.nc $PREPDIR/.
    mv grids.nc $PREPDIR/.
    mv masks.nc $PREPDIR/.
    #
    python3 $RUNDIR/SCRIPTS/OASIS_RESTART.py $PREPDIR $PREP_DATE
  fi
  #
fi
#
#=========================================================================
exit 0
#=========================================================================
