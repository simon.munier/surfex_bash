#!/bin/bash
#=========================================================================
#
# Script to run CTRIP with forcings at a monthly time step
#
# USAGE
#   ./script_trip.sh EXP
#
# by default: EXP=trip
#
#=========================================================================
#set -x

REGION=Spain
EXP=${1:-trip}

# Add option suffix
OPT=""
#grep -q 'CGROUNDW.*DIF' OPTIONS/TRIP_OPTIONS.nam && GW="_gw" || GW=""
#grep -q 'LFLOOD.*T' OPTIONS/TRIP_OPTIONS.nam && FL="_fld" || FL=""
#grep -q 'CLAKE.*MLK' OPTIONS/TRIP_OPTIONS.nam && LK="_mlk" || LK=""
#OPT=$GW$FL$LK
EXP=${EXP}${OPT}

TRIPNAML=TRIP_OPTIONS.nam
FORCINGNAME=ISBA_DIAG_CUMUL

#============================================
# simulation time span
#============================================
YYYYMM_beg=200901
YYYYMM_end=200912
DDHH=0100

#============================================
# directories and links
#============================================
VER=LXifort-SFX-V9-0-MPIAUTO-OMP-O2-X0
TRUNKDIR=$HOME/src/SURFEX_V9_DEV/Surfex_Git2
RIVEXE=$TRUNKDIR/exe/TRIP_MASTER-$VER

SCRATCHDIR=$WORKDIR/SURFEX/$REGION
FORCINGDIR=/home/$(whoami)/SURFEX/$REGION/forcings #on Hendrix
RESULTSDIR=/home/$(whoami)/SURFEX/$REGION/$EXP     #on Hendrix

#============================================
# create directories and copy files
#============================================
RUNDIR=$(pwd)
mkdir -p $SCRATCHDIR/$EXP
rm -rf $SCRATCHDIR/RUN/$EXP
mkdir -p $SCRATCHDIR/RUN/$EXP
cd $SCRATCHDIR/RUN/$EXP

# get params
ln -s $RUNDIR/PGD_PREP/TRIP_PARAM${OPT}.nc TRIP_PARAM.nc
# get restart
ln -s $RUNDIR/PGD_PREP/TRIP_PREP${OPT}_${YYYYMM_beg}${DDHH}.nc TRIP_PREP.nc
# get option files
cp -f $RUNDIR/OPTIONS/$TRIPNAML TRIP_OPTIONS.nam
# get executables
ln -s $RUNDIR/SCRIPTS/GET_FORCING.sh .
ln -s $RUNDIR/SCRIPTS/PUT_RESULT.sh .
ln -s $RUNDIR/SCRIPTS/PUT_PREP.sh .
ln -s $RUNDIR/SCRIPTS/ADD_DATE.sh .
ln -s $RUNDIR/SCRIPTS/TRIP_OFFLINE.EXE .
ln -s $RIVEXE trip.exe

#============================================
# loop over months
#============================================
echo "***************************************"
echo "   READY TO START SIMULATION"
echo "       EXP: $EXP"
echo "       $YYYYMM_beg  ->  $YYYYMM_end"
echo "***************************************"
date > date_$EXP
YYYYMM=$YYYYMM_beg
TMAX=`grep '#SBATCH -t' TRIP_OFFLINE.EXE | awk '{print $3}' | awk -F ':' '{print 5+3600*$1+60*$2+$3}'`
while [ $YYYYMM -le $YYYYMM_end ]; do

  # get forcing for this month
  ./GET_FORCING.sh -r -n $FORCINGNAME -t ISBA_${YYYYMM::4}.tar $YYYYMM $SCRATCHDIR/FORCING_TRIP $FORCINGDIR
  ln -sf $SCRATCHDIR/FORCING_TRIP/${FORCINGNAME}_${YYYYMM}.nc TRIP_FORCING.nc
  YYYYMM_next_year=`./ADD_DATE.sh $YYYYMM 1year`
  [ $YYYYMM_next_year -le $YYYYMM_end ] && \
    ./GET_FORCING.sh -n $FORCINGNAME -t ISBA_${YYYYMM_next_year::4}.tar $YYYYMM_next_year $SCRATCHDIR/FORCING_TRIP $FORCINGDIR &

  sleep 1
  [ ${YYYYMM:4:2} -eq "01" ] && echo "--------------------"

  echo $YYYYMM
  YYYYMM_next=`./ADD_DATE.sh $YYYYMM 1month`

  # run TRIP_OFFLINE
  rm -f finished
  sbatch ./TRIP_OFFLINE.EXE $YYYYMM $SCRATCHDIR/$EXP &>/dev/null
  T=0 ; while [ ! -f finished ] && [ $T -lt $TMAX ]; do T=$((T+1)); sleep 1; done
  [ ! -f TRIP_RESTART.nc ] && echo "Error with offline ${YYYYMM}" && exit 1

  # TRIP_RESTART file is TRIP_PREP file for next month
  mv -f TRIP_RESTART.nc TRIP_PREP.nc
  [ ${YYYYMM_next:4:2} -eq 01 ] && cp TRIP_PREP.nc $SCRATCHDIR/$EXP/TRIP_PREP_${YYYYMM_next}${DDHH}.nc

  # save results (only first month of year)
  [ ${YYYYMM_next:4:2} -eq 01 ] && ./PUT_RESULT.sh $YYYYMM $SCRATCHDIR/$EXP $RESULTSDIR &

  YYYYMM=$YYYYMM_next

done

# save prep and results for last year if not complete
./PUT_PREP.sh $SCRATCHDIR/$EXP $RESULTSDIR &
[ ${YYYYMM_next:4:2} -ne 01 ] && YYYYMM=`./ADD_DATE.sh $YYYYMM -1month` && ./PUT_RESULT.sh $YYYYMM $SCRATCHDIR/$EXP $RESULTSDIR

# end simulation
date >> date_$EXP
echo "***************************************"
echo "   SIMULATION ENDS CORRECTLY"
echo "***************************************"
