# surfex_bash


## Description
Ensemble of bash scripts to run a SURFEX and/or CTRIP simulation.
Works for machines at CNRM.

## Author
Simon Munier (simon.munier@meteo.fr)
CNRM, Météo-France

## Installation
To get the scripts for a new experiment named, e.g., Spain, type:

    git clone https://gitlab.com/simon.munier/surfex_bash.git Spain

Then use `change_host` to adapt the scripts to the current machine:

    ./change_host MACHINE

with `MACHINE` being `ubuntu`, `belenos` or `aneto`.

## Misc
For any new experiment:
- change extent in OPTIONS.nam and TRIP_OPTIONS.nam
- check all options in namelists (including namcouple if needed)
- check forcings and results directories on hendrix in script_*.sh
